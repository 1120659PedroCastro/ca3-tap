package isep.tap.ca3.tests

import org.scalatest.FunSuite
import isep.tap.ca3.model.Recipe
import scala.io.Source
import com.github.tototoshi.csv.CSVReader
import java.io.File
import isep.tap.ca3.dataReader.DataReaderImplCSV
import isep.tap.ca3.dataReader.DataReaderImplJSON
import isep.tap.ca3.dataReader.DataReader
import isep.tap.ca3.dataReader.StrategyDataReader
import isep.tap.ca3.dataReader.ObjectDataReader

class Exercise3Test extends FunSuite {

  test("Exercise 3 initial conditions...") {
    val dataReader = new DataReaderImplCSV

    val expected = List(Recipe("Portuguese Green Soup", "Portugal"), Recipe("Grilled Sardines", "Portugal"), Recipe("Salted Cod with Cream", "Portugal"))
    val other_expected = List(Recipe("German Sauerkraut", "Germany"), Recipe("Risotto", "Italy"), Recipe("Tortilla", "Spain"))

    assert(dataReader.readDataFast("recipe.csv").lr === expected)
    assert(dataReader.readDataSlow("other_recipe.csv").lr === other_expected)
  }

  /* TODO: create the tests */

  // strategy does not exist
  test("Exercise 3. Test 1.") {
    val dataReaderDocx = ObjectDataReader("recipe.docx")
    assert(dataReaderDocx === None)
  }

  // read one file JSON and another csv, with same content, verify equality
  test("Exercise 3. Test 2.") {
    val dataReaderCSV = new StrategyDataReader(ObjectDataReader("recipe.csv"))
    val dataReaderJSON = new StrategyDataReader(ObjectDataReader("recipe.json"))
    assert(dataReaderJSON.read("recipe.json").get.lr === dataReaderCSV.read("recipe.csv").get.lr)

  }

  // read one file JSON and another csv, with different content, verify inequality
  test("Exercise 3. Test 3.") {
    val dataReaderCSV = new StrategyDataReader(ObjectDataReader("recipe.csv"))
    val dataReaderJSON = new StrategyDataReader(ObjectDataReader("recipe.json"))
    assert(!(dataReaderJSON.read("recipe.json").get.lr === dataReaderCSV.read("other_recipe.csv").get.lr))

  }
}