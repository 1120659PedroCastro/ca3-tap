package isep.tap.ca3.crosscutting.profile

import isep.tap.ca3.dataReader.DataReader


/*
 * TODO: create the Profiler trait, which forces the reader to return a ProfilerList
 */

trait Profiler extends DataReader{
abstract override def readDataFast(fname: String): ProfilerList[Long] = {
    val initTime = System.currentTimeMillis()
    val result = super.readDataFast(fname)
    val pt = System.currentTimeMillis() - initTime
    
    return new ProfilerList[Long](result.lr,pt);  
  }

abstract override def readDataSlow(fname: String): ProfilerList[Long] = {
    val initTime = System.currentTimeMillis()
    val result = super.readDataSlow(fname)
    val pt = System.currentTimeMillis() - initTime
    
    return new ProfilerList[Long](result.lr,pt);  
  }
}
