package isep.tap.ca3.crosscutting.log

import isep.tap.ca3.logger.TimeTagLogger
import java.util.Calendar

/*
 * TODO: create the LogAdapter object
 */

package object LogAdapter {
  implicit class AppLoggerImplicit(timeLogger: TimeTagLogger) extends Log {
    override def log(message: String): Unit = timeLogger.log(message, Calendar.getInstance().getTime().toString())
    override def top(): (String, String) = timeLogger.s.top
  }
}