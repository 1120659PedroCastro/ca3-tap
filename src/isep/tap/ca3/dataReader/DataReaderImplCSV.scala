package isep.tap.ca3.dataReader


import isep.tap.ca3.model.Recipe
import scala.io.Source
import com.github.tototoshi.csv.CSVReader
import java.io.File

class DataReaderImplCSV extends DataReader{
  
   
  override def read(fname: String): RecipeList ={
    val pwd = CSVReader.open(new File(System.getProperty("user.dir")+"/files/"+fname))
    val file = pwd.all() 
    pwd.close()
    val data = file.slice(1, file.length)
    val ret = data.map(el => new Recipe(el.head, el.tail.head))
    new RecipeList(ret)
  }
}