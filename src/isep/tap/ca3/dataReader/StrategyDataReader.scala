package isep.tap.ca3.dataReader

 class StrategyDataReader (strategy: Option[(String) => RecipeList]) 
{
  def read(file: String) : Option[RecipeList] =
  {
    strategy match
    {
      case Some(strategy) => Some(strategy(file)) 
      case _ => None
    }
  }
}
