package isep.tap.ca3.dataReader

import net.liftweb.json._
import scala.io.Source
import isep.tap.ca3.model.Recipe


class DataReaderImplJSON extends DataReader {
  
  implicit val formats = DefaultFormats
  
  override def read(fname: String): RecipeList = {
    val pwd:String = System.getProperty("user.dir")+"/files/"+fname
    val file = Source.fromFile(pwd)
    val ret = parse(file.getLines.mkString).extract[List[Recipe]]
    file.close()
    new RecipeList(ret)
  }
  
}