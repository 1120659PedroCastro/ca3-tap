package isep.tap.ca3.dataReader

import net.liftweb.json.DefaultFormats
import com.github.tototoshi.csv.CSVReader
import java.io.InputStreamReader

object ObjectDataReader
{
  def apply(filename: String): Option[ (String) => RecipeList ] = 
    filename match 
    {
      case f if f.endsWith(".json") => Some(parseJson) 
      case f if f.endsWith(".csv") => Some(parseCsv) 
      case f => None
    }
  
  def parseJson(file: String): RecipeList = {
    val dataReader = new DataReaderImplJSON
    dataReader.read(file)
  }
  
  def parseCsv(file: String): RecipeList = {
    val dataReader = new DataReaderImplCSV
    dataReader.read(file)
  }
}